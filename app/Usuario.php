<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = "usuario";

    protected $username = 'username';

    protected $fillable = [
        'nombre', 'correo', 'contraseña',
    ];

    protected $hidden = [
        'contraseña', 'remember_token',
    ];

}
